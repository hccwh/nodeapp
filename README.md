# Nodejs的购物系统

#### 介绍
nodeJSHC  基于nodeJS express和数据库Mysql的简单购物系统  

### 界面

#### 后台

| ![](https://images.gitee.com/uploads/images/2020/1223/102217_6985c819_5004132.png) | ![](https://images.gitee.com/uploads/images/2020/1223/102400_9b35e173_5004132.png) |
| ------------------------------------------------- | ------------------------------------------------- |
| ![](https://images.gitee.com/uploads/images/2020/1223/102425_0a3e7a1a_5004132.png) | ![](https://images.gitee.com/uploads/images/2020/1223/102437_c82aaac4_5004132.png) |

#### 前端

| ![](https://images.gitee.com/uploads/images/2020/1223/102455_c1853c1e_5004132.png) | ![](https://images.gitee.com/uploads/images/2020/1223/102506_5ed58450_5004132.png) |
| ------------------------------------------------- | ------------------------------------------------- |
| ![](https://images.gitee.com/uploads/images/2020/1223/102519_195b3126_5004132.png) | ![](https://images.gitee.com/uploads/images/2020/1223/102532_596279dd_5004132.png) |

### 启动

1.  导入sql文件

2.  修改代码数据库配置（密码）

3. 下载依赖 npm i

4. 启动项目 npm run start



   + 如有不懂，可联系我！如果这个项目对您有帮助，请作者喝杯咖啡吧（发放技术介绍文档）。

     

| ![作者微信](wxCode.jpg) | ![赞赏码](skm.jpg) |
| :----------------------------------------------------------: | :----------------------------------------------------------: |

   