layui.define(function (exports) {

    var config = {
        base_server: '/admin/', // 接口地址，实际项目请换成http形式的地址
        tableName: 'easyweb',  // 存储表名
        autoRender: false,  // 窗口大小改变后是否自动重新渲染表格，解决layui数据表格非响应式的问题，目前实现的还不是很好，暂时关闭该功能
        pageTabs: true,   // 是否开启多标签
        // 获取缓存的token
        getToken: function () {
            var t = layui.data(config.tableName).token;
            if (t) {
                return JSON.parse(t);
            }
        },
        // 清除user
        removeToken: function () {
            layui.data(config.tableName, {
                key: 'token',
                remove: true
            });
        },
        // 缓存token
        putToken: function (token) {
            layui.data(config.tableName, {
                key: 'token',
                value: JSON.stringify(token)
            });
        },
        // 导航菜单，最多支持三级，因为还有判断权限，所以渲染左侧菜单很复杂，无法做到递归
        menus: [
            {
                name: '主页',
                icon: 'layui-icon-home',
                url: '#!console',
                path: "../components/console.html",
                hiden: true
            },
            {
                name: '用户管理',
                icon: 'layui-icon-username',
                url: '#!userTable',
                path: '../components/system/userTable.html'
            },
            {
                name: '管理员管理',
                icon: 'layui-icon-friends',
                url: '#!adminTable',
                path: '../components/system/adminTable.html'
            },
            {
                name: '首页轮播图',
                icon: 'layui-icon-picture',
                url: '#!swiperTable',
                path: '../components/system/swiperTable.html'
            },
            {
                icon: 'layui-icon-star',
                name: '手办管理',
                url: '#!goodsTable',
                path: '../components/system/goodsTable.html'
            },
            {
                icon: 'layui-icon-release',
                name: '订单管理',
                url: '#!orderTable',
                path: '../components/system/orderTable.html'
            }
        ],
        // 当前登录的用户
        getUser: function () {
            var u = layui.data(config.tableName).login_user;
            if (u) {
                return JSON.parse(u);
            }
        },
        // 缓存user
        putUser: function (user) {
            layui.data(config.tableName, {
                key: 'login_user',
                value: JSON.stringify(user)
            });
        }
    };
    exports('config', config);
});
