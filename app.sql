/*
Navicat MySQL Data Transfer

Source Server         : 180.76.119.18
Source Server Version : 50529
Source Host           : localhost:3306
Source Database       : app_two

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2021-05-23 12:36:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT '女',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('3', '1', '1', 'http://127.0.0.1:3000/public/img/admin/woman.png', '男');
INSERT INTO `admin` VALUES ('7', '2', '2', 'http://127.0.0.1:3000/public/img/admin/man.png', '男');

-- ----------------------------
-- Table structure for `card`
-- ----------------------------
DROP TABLE IF EXISTS `card`;
CREATE TABLE `card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `orderDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `orderState` varchar(11) DEFAULT NULL,
  `comm` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card
-- ----------------------------

-- ----------------------------
-- Table structure for `comm`
-- ----------------------------
DROP TABLE IF EXISTS `comm`;
CREATE TABLE `comm` (
  `commId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `goodsId` int(11) DEFAULT NULL,
  `commText` varchar(255) NOT NULL,
  `commDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`commId`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comm
-- ----------------------------
INSERT INTO `comm` VALUES ('1', '1', '5', '111', '2021-05-13 23:19:26');
INSERT INTO `comm` VALUES ('2', '1', '5', '222', '2021-05-13 23:20:39');
INSERT INTO `comm` VALUES ('3', '1', '5', '商品不错', '2021-05-13 23:32:06');
INSERT INTO `comm` VALUES ('4', '1', '5', '1111', '2021-05-15 10:02:19');

-- ----------------------------
-- Table structure for `commodity`
-- ----------------------------
DROP TABLE IF EXISTS `commodity`;
CREATE TABLE `commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `urlList` varchar(9999) DEFAULT NULL,
  `porice` varchar(255) DEFAULT NULL,
  `stock` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of commodity
-- ----------------------------
INSERT INTO `commodity` VALUES ('7', '黑saber', 'http://127.0.0.1:3000/public/img/admin/goods/1619680070743-src=http___img10.360buyimg.com_n1_jfs_t1_142789_10_9922_311672_5f7943d8E67677fac_f98d3035cb846f31.jpg&refer=http___img10.360buyimg.jfif', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619680070743-src=http___img10.360buyimg.com_n1_jfs_t1_142789_10_9922_311672_5f7943d8E67677fac_f98d3035cb846f31.jpg&refer=http___img10.360buyimg.jfif\"]', '60', '60', '有着精致的做工和鲜艳的颜色，便宜卖出');
INSERT INTO `commodity` VALUES ('5', '女仆亚丝娜', 'http://127.0.0.1:3000/public/img/admin/goods/1619577130163-1.png', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619577130163-1.png\"]', '99', '20', '由刀剑神域中的亚丝娜所扮演的可爱女仆');
INSERT INTO `commodity` VALUES ('6', '泳装蕾姆', 'http://127.0.0.1:3000/public/img/admin/goods/1619577179625-2.png', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619577179625-2.png\"]', '99', '10', '炎炎夏日中的蕾姆');
INSERT INTO `commodity` VALUES ('8', '花嫁尼禄', 'http://127.0.0.1:3000/public/img/admin/goods/1619680217735-src=http___img13.360buyimg.com_n1_jfs_t1_160443_19_16634_160232_606bd00bE070d78ad_d2c43d20a486ef90.jpg&refer=http___img13.360buyimg.jfif', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619680217735-src=http___img13.360buyimg.com_n1_jfs_t1_160443_19_16634_160232_606bd00bE070d78ad_d2c43d20a486ef90.jpg&refer=http___img13.360buyimg.jfif\"]', '950', '3', '比市场价便宜不少，虽然已拆装，但是并没有损坏和擦伤，开箱时间只有2天而已');
INSERT INTO `commodity` VALUES ('9', '创世神亚丝娜', 'http://127.0.0.1:3000/public/img/admin/goods/1619680356287-src=http___n.sinaimg.cn_sinakd20200629ac_160_w590h370_20200629_581d-ivrxcex1851002.jpg&refer=http___n.sinaimg.jfif', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619680356287-src=http___n.sinaimg.cn_sinakd20200629ac_160_w590h370_20200629_581d-ivrxcex1851002.jpg&refer=http___n.sinaimg.jfif\"]', '250', '1', '在刀剑神域第三部出场的角色，是少见的创世神角色，新品。');
INSERT INTO `commodity` VALUES ('10', '呆毛王', 'http://127.0.0.1:3000/public/img/admin/goods/1619680466322-src=http___www.62a.net_tbimg_img_imgextra_i3_23708940_TB2AvnVaA9WBuNjSspeXXaz5VXa_!!23708940.jpg&refer=http___www.62a.jfif', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619680466322-src=http___www.62a.net_tbimg_img_imgextra_i3_23708940_TB2AvnVaA9WBuNjSspeXXaz5VXa_!!23708940.jpg&refer=http___www.62a.jfif\"]', '600', '2', 'fate系列的看板娘角色，是吾等公认的王，新出，晚下单就没有了');
INSERT INTO `commodity` VALUES ('11', '接头霸王系列赛车', 'http://127.0.0.1:3000/public/img/admin/goods/1619680569077-u=704624182,3028149408&fm=26&gp=0.jpg', '[\"http://127.0.0.1:3000/public/img/admin/goods/1619680569077-u=704624182,3028149408&fm=26&gp=0.jpg\"]', '40', '6', '由XX公司出厂的街头霸王系列的赛车，40一个尺寸为30cm');

-- ----------------------------
-- Table structure for `swiper`
-- ----------------------------
DROP TABLE IF EXISTS `swiper`;
CREATE TABLE `swiper` (
  `swiperId` int(11) NOT NULL AUTO_INCREMENT,
  `swiperurl` varchar(255) NOT NULL,
  `swiperIndex` int(11) DEFAULT '0',
  `swipertime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`swiperId`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of swiper
-- ----------------------------
INSERT INTO `swiper` VALUES ('1', 'http://127.0.0.1:3000/public/img/user/swiper/1619573999692-gaoda.png', '1', '2021-05-22 12:54:33');
INSERT INTO `swiper` VALUES ('2', 'http://127.0.0.1:3000/public/img/user/swiper/1619574006452-lily.png', '2', '2021-05-22 12:54:35');
INSERT INTO `swiper` VALUES ('3', 'http://127.0.0.1:3000/public/img/user/swiper/1619574162617-1619574162000.jpeg', '3', '2021-05-22 12:54:35');
INSERT INTO `swiper` VALUES ('4', 'http://127.0.0.1:3000/public/img/user/swiper/1619574022633-nilu.png', '4', '2021-05-22 12:54:37');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `userTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `avator` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', '1778311645111@qq.com', '2020-12-12 14:30:58', 'http://127.0.0.1:3000/public/upload/1620834318661-339457269e85be7bbc462cd902332686.jpg');
