var express = require('express');
var router = express.Router();
var mysql = require('../mysql/fun');
var createError = require('http-errors');
var multer = require('multer');
const session = require('express-session');
var { changeStock, getGoodsByNameCount } = require("../mysql/admin")
var storage = multer.diskStorage({
  //将上传的文件存储在指定的位置（不存在的话需要手动创建）
  destination: function (req, file, cb) {
    cb(null, 'public/upload')
  },
  //将上传的文件做名称的更改
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname)
  }
})
const upload = multer({ storage: storage })
/**
 * 所有的api请求
 */
router.post('/login', function (req, res, next) {
  var bodys = req.body;
  var username = bodys.username;
  var password = bodys.password;
  //判断验证码
  // var sessionCaptcha = req.session.captcha.toLowerCase();
  // if (sessionCaptcha != bodys.verify) {
  //   return res.send({
  //     flog: 0,
  //     data: '验证码不正确'
  //   });
  // };
  mysql.login(username, password, function (err, data) {
    if (err) {
      next("未知错误");
    }
    if (!data.length) {
      return res.json({
        flog: 0,
        data: '输入信息有误，请重新输入'
      });
    } else {
      req.session.username = username;
      req.session.avator = data[0].avator;
      req.session.userId = data[0].userId;
      res.json({
        flog: 1,
        data: data[0]
      });
    }
  })
});
router.post('/register', function (req, res, next) {
  var bodys = req.body;
  mysql.isuser(bodys.name, function (err, data) {
    if (err) {
      return next(err);
    }
    if (!data.code) {
      return res.json({
        flog: 0,
        data: '服务器已经存在该用户'
      });
    } else {
      mysql.register({
        username: bodys.name,
        password: bodys.pass,
        email: bodys.email
      }, function (err, data) {
        if (err) {
          return next(err);
        }
        if (data) {
          req.session.username = bodys.name;
          return res.json({
            flog: 1,
            data: '注册用户成功'
          });
        } else {
          return res.json({
            flog: 0,
            data: '注册用户失败'
          });
        }
      })
    }
  })
});
router.post('/dellogin', function (req, res, next) {
  req.session.destroy(function (err) {
    if (err) {
      return res.json({
        flog: 0,
        data: '退出失败'
      });
    }
    res.clearCookie("user_key");
    return res.json({
      flog: 1,
      data: '退出成功'
    });
  })
});
router.get('/islogin', function (req, res, next) {
  if (req.session.username) {
    return res.json({
      flog: 1,
      data: '已经登录'
    });
  } else {
    return res.json({
      flog: 0,
      data: '未登录'
    });
  }
});
router.get('/shoppinfo', function (req, res, next) {
  var id = req.query.id;
  mysql.commoditybyid(id, function (err, data) {
    if (err) {
      return res.json({
        flog: 0,
        data: '获得信息失败'
      });
    }
    return res.json({
      flog: 1,
      data: data
    });
  });
});
router.get('/commodityall', function (req, res, next) {
  mysql.commodityall(function (err, data) {
    if (err) {
      return res.json({
        flog: 0,
        data: '获得信息失败'
      });
    }
    return res.json({
      flog: 1,
      data: data
    });
  })
})
router.get('/commodityPage', async function (req, res, next) {
  let { page, limit, name } = req.query;
  let count = await getGoodsByNameCount(name);
  let pageNum = Math.ceil((count / limit));
  if (page > pageNum) {
    return res.json({
      code: 1,
      data: [],
      msg: "数据已经加载完毕"
    });
  }
  var num = (page - 1) * limit;
  mysql.commodityPage({
    num,
    limit,
    name
  }, function (err, data) {
    if (err) {
      return res.json({
        flog: 0,
        data: '获得信息失败'
      });
    }
    return res.json({
      flog: 1,
      data: data,
      pageNum: pageNum
    });
  })
})
router.get('/commoditybyname', function (req, res, next) {
  var name = req.query.name;
  mysql.commoditybyname(name, function (err, data) {
    if (err) {
      return res.json({
        flog: 0,
        data: '获得信息失败'
      });
    }
    return res.json({
      flog: 1,
      data: data
    });
  });
})
router.post('/addshopcardinfo', function (req, res, next) {
  mysql.addshopcardinfo({
    username: req.session.username,
    userId: req.session.userId,
    comm: req.body.data,
    orderState: 0
  }, async function (err, data) {
    if (err) {
      return next("未知错误");
    }
    //减少库存
    let prList = JSON.parse(req.body.data).map(async (pro) => {
      await changeStock(pro.id, {
        stock: -Number(pro.number)
      })
    })
    await Promise.all(prList)
    return res.json({
      flog: 1,
      data: '添加手办成功'
    });
  })
})
router.get('/delcard', function (req, res, next) {
  var id = req.query.id;
  mysql.showcardbyid(id, function (err, data) {
    let comm = JSON.parse(data[0].comm)
    mysql.delcardbyid(id, async function (err, data) {
      if (err) {
        return next("未知错误");
      }
      //增加库存
      let prList = comm.map(async (pro) => {
        await changeStock(pro.id, {
          stock: Number(pro.number)
        })
      })
      await Promise.all(prList)
      return res.json({
        flog: 1,
        data: '删除订单成功'
      });

    })
  })
})
router.get('/delivercard', function (req, res, next) {
  var id = req.query.id;
  mysql.delivercard(id, function (err, data) {
    if (err) {
      return next("未知错误");
    }
    return res.json({
      flog: 1,
      data: '收货成功'
    });
  })
})
router.get('/returncard', function (req, res, next) {
  var id = req.query.id;
  mysql.returncard(id, function (err, data) {
    if (err) {
      return next("未知错误");
    }

    mysql.showcardbyid(id, async function (err, data) {
      //增加库存
      let prList = JSON.parse(data[0].comm).map(async (pro) => {
        await changeStock(pro.id, {
          stock: Number(pro.number)
        })
      })
      await Promise.all(prList)
    })

    return res.json({
      flog: 1,
      data: '退货成功'
    });
  })
})
router.post('/userInfo', function (req, res, next) {
  var userId = req.session.userId;
  mysql.getUserInfo(userId, function (err, data) {
    if (err) {
      return next("未知错误");
    }
    return res.json({
      flog: 1,
      data: data
    });
  })
})
router.post('/updateUserInfo', upload.single("avator"), function (req, res, next) {
  let { userId, username, email, avator } = req.body;
  avator = req.file ? (`${req.protocol}://${req.headers.host}/${req.file.path}`).replace(/\\/ig, '/') : '';
  console.log(userId, username, email, avator)
  mysql.updateUserInfo({
    userId, username, email, avator
  }, function (err, data) {
    if (err) {
      return next("未知错误");
    }
    req.session.username = username;
    if (avator) {
      req.session.avator = avator;
    }
    return res.json({
      flog: 1,
      data: '修改成功'
    });
  })
})
router.post('/addComm', function (req, res, next) {
  var bodys = req.body;
  console.log(session)
  mysql.addComm({
    commText: bodys.commText,
    goodsId: bodys.goodsId,
    userId: req.session.userId
  }, function (err, data) {
    if (err) {
      return next(err);
    }
    if (data) {
      return res.json({
        flog: 1
      });
    } else {
      return res.json({
        flog: 0
      });
    }
  })
});
router.post('/UpdateUserPassById', async (req, res, next) => {
  let { oldPsw, newPsw } = req.body;
  let userId = req.session.userId;
  let username = req.session.username;
  mysql.login(username, oldPsw, function (err, data) {
    if (err) {
      next("未知错误");
    }
    if (!data.length) {
      return res.json({
        flog: 0,
        mag: '旧密码错误'
      });
    } else {

      mysql.UpdateUserPassById({
        userId: userId,
        password: newPsw
      }, function (err, data) {
        if (err) {
          return next("未知错误");
        }
        return res.json({
          flog: 1,
          msg: "修改密码成功"
        })
      })
    }
  })
})
/**
 * 处理所有api错误
 */
router.post('/personal', function (req, res, next) {
  var bodys = req.body;
  var username = bodys.username;
  var password = bodys.password;
  console.log("测试开始")
  console.log(req.session)
  console.log("测试结束")
  mysql.login(username, password, function (err, data) {
    if (err) {
      next("未知错误");
    }
    if (!data.length) {
      return res.json({
        flog: 0,
        data: '输入信息有误，请重新输入'
      });
    } else {

      req.session.username = username;
      req.session.avator = data[0].avator;
      res.json({
        flog: 1,
        data: data[0]
      });
    }
  })
});

router.use(function (err, req, res, next) {
  console.log(err)
  return res.send({
    flog: 0,
    data: err
  })
});

module.exports = router;